export const nettoyerTexte = (str: string) => {
  return str.replace(/\s+$/gm, "");
};

export const dateToString = (d: Date) => {
  return d.toLocaleDateString("fr-fr", {
    weekday: "short",
    year: "2-digit",
    month: "short",
    day: "numeric",
  });
};

export const getHourFromDate = (d: Date): string => {
  return [d.getHours(), d.getMinutes()]
    .map((x) => x.toString(10).padStart(2, "0"))
    .join(":");
};

export const dateTimeStrToDate = (dateTimeStr: string): Date => {
  const timestamp: number = Date.parse(dateTimeStr);
  return new Date(timestamp);
};

export const getFullDate = (d: Date): string => {
  return `${dateToString(d)} - ${getHourFromDate(d)}`;
};
