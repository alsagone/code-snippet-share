import { dateTimeStrToDate, nettoyerTexte } from "~/scripts/helper";

export class Post {
  id: string;
  titre: string;
  contenu: string;
  auteur: string;
  auteurID: string;
  langage: string;
  dateStr: string;
  datePublication: Date;
  scoreVote: number;

  constructor(
    id: string,
    titre: string,
    contenu: string,
    auteur: string,
    auteurID: string,
    dateStr: string,
    langage: string,
    scoreVote: number
  ) {
    this.id = id;
    this.titre = titre;
    this.contenu = nettoyerTexte(contenu);
    this.auteur = auteur;
    this.auteurID = auteurID;
    this.langage = langage;
    this.dateStr = dateStr;
    this.datePublication = dateTimeStrToDate(dateStr);
    this.scoreVote = scoreVote;
  }

  getCodeAffiche(nbLignes: number | undefined): string {
    if (!nbLignes) {
      return this.contenu;
    }

    const lignes = this.contenu.split("\n");

    return nbLignes < lignes.length
      ? lignes.slice(0, nbLignes).join("\n") + "\n..."
      : this.contenu;
  }

  compareDate(autrePost: Post, plusRecent: boolean = true): number {
    /**
     * Fonction de comparaison de dates entre deux posts
     *
     * @remarks
     * Cette fonction sera utilisée pour trier un tableau d'objets Post (arr.sort((p1, p2) => p1.compareDate(p2)))
     *
     * @param autrePost - un objet de type Post
     * @param plusRecent - booléen qui permet de trier les posts du plus récent au plus ancien, vrai par défaut
     * @returns 1, -1 ou 0
     *
     *
     */
    if (this.datePublication === autrePost.datePublication) {
      return this.titre > autrePost.titre ? 1 : -1;
    }

    const signe: number = plusRecent ? -1 : 1;
    const resultat: number =
      this.datePublication > autrePost.datePublication ? 1 : -1;
    return signe * resultat;
  }

  compareVotes(autrePost: Post): number {
    if (this.scoreVote === autrePost.scoreVote) {
      return this.compareDate(autrePost, true);
    }

    return this.scoreVote < autrePost.scoreVote ? 1 : -1;
  }
}
