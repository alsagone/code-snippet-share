<?php
include_once '../../config/database.php';
include_once '../../modeles/utilisateur.php';

// Headers requis
// Accès depuis n'importe quel site ou appareil (*)
header("Access-Control-Allow-Origin: *");

// On renvoie du JSON
header("Content-Type: application/json; charset=UTF-8");

// Méthode autorisée: POST
header("Access-Control-Allow-Methods: POST");

// Durée de vie de la requête
header("Access-Control-Max-Age: 3600");

// Entêtes autorisées
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$erreur = false;

if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    http_response_code(405);
    echo json_encode(["message" => "La méthode n'est pas autorisée"]);
    $erreur = true;
}

$parametres_requis = ["id_utilisateur"];

$message = "";

foreach ($parametres_requis as $parametre) {
    if (!isset($_POST[$parametre])) {
        http_response_code(405);
        $message .= "Paramètre " . $parametre . " manquant\n";
        $erreur = true;
    }
}

if ($erreur) {
    echo json_encode(["message" => $message]);
    exit(1);
}


$database = new Database();
$db = $database->getConnexion();


$utilisateur = new Utilisateur($db);

$utilisateur->id_utilisateur = strip_tags($_POST['id_utilisateur']);

if ($_FILES["photo"]) {
    $utilisateur->photo = file_get_contents($_FILES["photo"]['tmp_name']);
}

if ($utilisateur->editer_utilisateur() == 0) {
    http_response_code(201);
    echo json_encode(["message" => "Utilisateur édité"]);
} else {
    http_response_code(503);
    echo json_encode(["message" => "Echec de l'édition de l'utilisateur"]);
}
