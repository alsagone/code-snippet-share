<?php
include_once '../../config/database.php';
include_once '../../modeles/utilisateur.php';


// Headers requis
// Accès depuis n'importe quel site ou appareil (*)
header("Access-Control-Allow-Origin: *");

// On renvoie du JSON
header("Content-Type: application/json; charset=UTF-8");

// Méthode autorisée: GET
header("Access-Control-Allow-Methods: GET");

// Durée de vie de la requête
header("Access-Control-Max-Age: 3600");

// Entêtes autorisées
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$erreur = false;

if ($_SERVER['REQUEST_METHOD'] != 'GET') {
    http_response_code(405);
    echo json_encode(["message" => "La méthode n'est pas autorisée"]);
    $erreur = true;
}

$parametres_requis = ["nom_utilisateur"];

foreach ($parametres_requis as $parametre) {
    if (!isset($_GET[$parametre])) {
        http_response_code(405);
        echo json_encode(["message" => "Paramètre " . $parametre . " manquant"]);
        $erreur = true;
    }
}

if ($erreur) {
    exit(1);
}


$database = new Database();
$db = $database->getConnexion();


$utilisateur = new Utilisateur($db);
$utilisateur->nom_utilisateur = strip_tags($_GET["nom_utilisateur"]);
$details = $utilisateur->get_details();

if (!$details) {
    http_response_code(405);
    echo json_encode(["message" => "La requête n'a rien renvoyé"]);
    exit(1);
}


while ($row = $details->fetch(PDO::FETCH_ASSOC)) {
    extract($row);
    $resultat = [
        "nom_utilisateur" => $utilisateur->nom_utilisateur,
        "id_utilisateur" => $row["ID_UTILISATEUR"],
        "adresse_mail" => $row["ADRESSE_MAIL"],
        "photo" => base64_encode($row["PHOTO"]),
        "date_inscription" => $row["DATE_INSCRIPTION"]
    ];
}

// On envoie le code réponse 200 OK
http_response_code(200);

// On encode en json et on envoie
echo json_encode($resultat);
