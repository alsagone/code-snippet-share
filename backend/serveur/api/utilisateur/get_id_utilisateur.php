<?php
include_once '../../config/database.php';
include_once '../../modeles/utilisateur.php';


// Headers requis
// Accès depuis n'importe quel site ou appareil (*)
header("Access-Control-Allow-Origin: *");

// On renvoie du JSON
header("Content-Type: application/json; charset=UTF-8");

// Méthode autorisée: GET
header("Access-Control-Allow-Methods: GET");

// Durée de vie de la requête
header("Access-Control-Max-Age: 3600");

// Entêtes autorisées
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$erreur = false;

if ($_SERVER['REQUEST_METHOD'] != 'GET') {
    http_response_code(405);
    echo json_encode(["message" => "La méthode n'est pas autorisée"]);
    $erreur = true;
}

$parametres_requis = ["nom_utilisateur"];

foreach ($parametres_requis as $parametre) {
    if (!isset($_GET[$parametre])) {
        http_response_code(405);
        echo json_encode(["message" => "Paramètre " . $parametre . " manquant"]);
        $erreur = true;
    }
}

if ($erreur) {
    exit(1);
}


$database = new Database();
$db = $database->getConnexion();


$utilisateur = new Utilisateur($db);
$utilisateur->nom_utilisateur = htmlspecialchars(strip_tags($_GET['nom_utilisateur']));

$id_utilisateur = $utilisateur->get_id_utilisateur();

if ($id_utilisateur != null) {
    http_response_code(201);
    echo json_encode(["reponse" => $id_utilisateur]);
} else {
    http_response_code(503);
    echo json_encode(["reponse" => "id_utilisateur non trouvé"]);
}
