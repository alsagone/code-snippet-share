<?php
include_once '../../config/database.php';
include_once '../../modeles/post.php';

// Headers requis
// Accès depuis n'importe quel site ou appareil (*)
header("Access-Control-Allow-Origin: *");

// On renvoie du JSON
header("Content-Type: application/json; charset=UTF-8");

// Méthode autorisée: POST
header("Access-Control-Allow-Methods: POST");

// Durée de vie de la requête
header("Access-Control-Max-Age: 3600");

// Entêtes autorisées
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$erreur = false;

if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    http_response_code(405);
    echo json_encode(["message" => "La méthode n'est pas autorisée"]);
    $erreur = true;
}

$parametres_requis = ["id_post", "id_utilisateur"];

$message = "";

foreach ($parametres_requis as $parametre) {
    if (!isset($_POST[$parametre])) {
        http_response_code(405);
        $message .= "Paramètre " . $parametre . " manquant\n";
        $erreur = true;
    }
}

if ($erreur) {
    echo json_encode(["message" => $message]);
    exit(1);
}

$database = new Database();
$db = $database->getConnexion();

$post = new Post($db);

$post->id_post = htmlspecialchars(strip_tags($_POST['id_post']));
$id_utilisateur = htmlspecialchars(strip_tags($_POST['id_utilisateur']));

if ($post->upvoter($id_utilisateur) == 0) {
    http_response_code(201);
    echo json_encode(["message" => "Post upvoté"]);
} else {
    http_response_code(503);
    echo json_encode(["message" => "Echec de l'upvote du post"]);
}