<?php
include_once '../../config/database.php';
include_once '../../modeles/post.php';

// Headers requis
// Accès depuis n'importe quel site ou appareil (*)
header("Access-Control-Allow-Origin: *");

// On renvoie du JSON
header("Content-Type: application/json; charset=UTF-8");

// Méthode autorisée: GET
header("Access-Control-Allow-Methods: GET");

// Durée de vie de la requête
header("Access-Control-Max-Age: 3600");

// Entêtes autorisées
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$erreur = false;

if ($_SERVER['REQUEST_METHOD'] != 'GET') {
    http_response_code(405);
    echo json_encode(["message" => "La méthode n'est pas autorisée"]);
    $erreur = true;
}

if ($erreur) {
    exit(1);
}

$database = new Database();
$db = $database->getConnexion();

$post = new Post($db);

$liste_posts = $post->liste_posts();

$tableauPosts = [];

while ($row = $liste_posts->fetch(PDO::FETCH_ASSOC)) {
    extract($row);

    $tableauPosts[] = [
        "id" => $row["ID_POST"],
        "titre" => $row["TITRE"],
        "contenu" => $row["CONTENU"],
        "langage" => $row["LANGAGE"],
        "auteurID" => $row["ID_AUTEUR"],
        "auteur" => $row["NOM_UTILISATEUR"],
        "dateStr" => $row["DATE_PUBLICATION"],
        "scoreVote" => $row["SCORE_VOTE"]
    ];

}

// On envoie le code réponse 200 OK
http_response_code(200);

// On encode en json et on envoie
echo json_encode($tableauPosts);


