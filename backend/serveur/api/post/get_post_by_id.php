<?php
include_once '../../config/database.php';
include_once '../../modeles/post.php';

// Headers requis
// Accès depuis n'importe quel site ou appareil (*)
header("Access-Control-Allow-Origin: *");

// On renvoie du JSON
header("Content-Type: application/json; charset=UTF-8");

// Méthode autorisée: GET
header("Access-Control-Allow-Methods: GET");

// Durée de vie de la requête
header("Access-Control-Max-Age: 3600");

// Entêtes autorisées
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$erreur = false;

if ($_SERVER['REQUEST_METHOD'] != 'GET') {
    http_response_code(405);
    echo json_encode(["message" => "La méthode n'est pas autorisée"]);
    $erreur = true;
}

$parametres_requis = ["id_post"];

$message = "";

foreach ($parametres_requis as $parametre) {
    if (!isset($_GET[$parametre])) {
        http_response_code(405);
        $message .= "Paramètre " . $parametre . " manquant\n";
        $erreur = true;
    }
}

if ($erreur) {
    exit(1);
}

$database = new Database();
$db = $database->getConnexion();

$post = new Post($db);
$post->id_post = strip_tags($_GET["id_post"]);
$p = $post->get_post_by_id();

if (!$p) {
    http_response_code(405);
    echo "Erreur requête";
    exit(1);
}
$resultat = [];

while ($row = $p->fetch(PDO::FETCH_ASSOC)) {
    extract($row);

    $resultat = [
        "id" => $row["ID_POST"],
        "titre" => $row["TITRE"],
        "contenu" => $row["CONTENU"],
        "langage" => $row["LANGAGE"],
        "auteurID" => $row["ID_AUTEUR"],
        "auteur" => $row["NOM_UTILISATEUR"],
        "dateStr" => $row["DATE_PUBLICATION"],
        "scoreVote" => $row["SCORE_VOTE"]
    ];

}

// On envoie le code réponse 200 OK
http_response_code(200);

// On encode en json et on envoie
echo json_encode($resultat);


