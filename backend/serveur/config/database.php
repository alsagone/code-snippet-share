<?php
class Database
{
    private $hote = "id21838919_hakim";
    private $login = "hakim";
    private $mot_de_passe = 'q!hQ=pO1Et3"Gz9l>0n';
    private $nom_base_de_donnees = "id21838919_code_snippet";
    public $connexion;

    public function getConnexion()
    {
        $this->connexion = null;

        try {
            $this->connexion = new PDO("mysql:host=" . $this->hote . ";dbname=" . $this->nom_base_de_donnees, $this->login, $this->mot_de_passe);
            $this->connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->connexion->exec("set names utf8");
        } catch (PDOException $exception) {
            echo "Erreur de connexion : " . $exception->getMessage();
        }

        return $this->connexion;
    }
}
