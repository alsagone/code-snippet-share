<?php
class Post
{
    private $connexion = null;
    public $id_post;
    public $titre;
    public $contenu;
    public $langage;
    public $id_auteur;
    public $date_publication;
    public $upvotes;
    public $downvotes;

    public function __construct($db)
    {
        $this->connexion = $db;
    }

    // Récupère toute la liste des posts dans la base de données
    public function liste_posts()
    {
        $statement = $this->connexion->prepare("SELECT p.ID_POST, p.TITRE, p.CONTENU, u.NOM_UTILISATEUR, p.LANGAGE, p.ID_AUTEUR, p.DATE_PUBLICATION, p.SCORE_VOTE FROM POST p INNER JOIN UTILISATEUR u ON p.ID_AUTEUR = u.ID_UTILISATEUR");

        try {
            $statement->execute();
        } catch (\PDOException $e) {
            return null;
        }

        return $statement;
    }


    public function inserer_post()
    {
        $statement = $this->connexion->prepare("INSERT INTO POST VALUES (UUID(), :titre, :contenu, :langage, :id_auteur, NOW(), 0)");
        $statement->bindValue('titre', $this->titre, PDO::PARAM_STR);
        $statement->bindValue('contenu', $this->contenu, PDO::PARAM_STR);
        $statement->bindValue('langage', $this->langage, PDO::PARAM_STR);
        $statement->bindValue('id_auteur', $this->id_auteur, PDO::PARAM_STR);

        try {
            $statement->execute();
        } catch (\PDOException $e) {
            return -1;
        }

        return 0;
    }

    public function supprimer_post()
    {
        $statement = $this->connexion->prepare("DELETE FROM POST p WHERE p.ID_POST = :id_post AND p.ID_AUTEUR= :id_auteur");
        $statement->bindValue('id_post', strip_tags($this->id_post), PDO::PARAM_STR);
        $statement->bindValue('id_auteur', strip_tags($this->id_auteur), PDO::PARAM_STR);


        try {
            $statement->execute();
        } catch (\PDOException $e) {
            return -1;
        }

        return 0;
    }

    public function editer_post()
    {
        $statement = $this->connexion->prepare("UPDATE POST p SET p.CONTENU = :contenu WHERE p.ID_POST = :id_post");
        $statement->bindValue('contenu', htmlspecialchars(strip_tags($this->contenu)), PDO::PARAM_STR);
        $statement->bindValue('id_post', htmlspecialchars(strip_tags($this->id_post)), PDO::PARAM_STR);

        try {
            $statement->execute();
        } catch (\PDOException $e) {
            return -1;
        }

        return 0;
    }

    public function voter_post($id_utilisateur, $valeur_vote)
    {
        $statement = $this->connexion->prepare("CALL VOTER_POST(:id_post, :id_utilisateur, :valeur_vote)");
        $statement->bindValue('id_post', htmlspecialchars(strip_tags($this->id_post)), PDO::PARAM_STR);
        $statement->bindValue('id_utilisateur', htmlspecialchars(strip_tags($id_utilisateur)), PDO::PARAM_STR);
        $statement->bindValue('valeur_vote', htmlspecialchars(strip_tags($valeur_vote)), PDO::PARAM_INT);

        try {
            $statement->execute();
        } catch (\PDOException $e) {
            return -1;
        }

        return 0;
    }

    public function upvoter($id_utilisateur)
    {
        $this->voter_post($id_utilisateur, 1);
    }

    public function downvoter($id_utilisateur)
    {
        $this->voter_post($id_utilisateur, -1);
    }

    public function get_post_by_id()
    {
        $statement = $this->connexion->prepare("SELECT p.ID_POST, p.TITRE, p.CONTENU, u.NOM_UTILISATEUR, p.LANGAGE, p.ID_AUTEUR, p.DATE_PUBLICATION, p.SCORE_VOTE FROM POST p INNER JOIN UTILISATEUR u ON p.ID_AUTEUR = u.ID_UTILISATEUR WHERE p.ID_POST = :id_post");
        $statement->bindValue('id_post', strip_tags($this->id_post), PDO::PARAM_STR);

        try {
            $statement->execute();
        } catch (\PDOException $e) {
            return null;
        }

        return $statement;
    }

    public function statut_vote($id_utilisateur)
    {
        $statement = $this->connexion->prepare("SELECT IFNULL((SELECT v.VALEUR FROM VOTE v WHERE v.ID_POST = :id_post AND v.ID_UTILISATEUR = :id_utilisateur), 0) as VALEUR;
");
        $statement->bindValue('id_post', strip_tags($this->id_post), PDO::PARAM_STR);
        $statement->bindValue('id_utilisateur', strip_tags($id_utilisateur), PDO::PARAM_STR);

        try {
            $statement->execute();
        } catch (\PDOException $e) {
            return 0;
        }

        return $statement;
    }

    public function get_posts_by_user($id_utilisateur)
    {
        $statement = $this->connexion->prepare("SELECT p.ID_POST, p.TITRE, p.LANGAGE, p.ID_AUTEUR, p.DATE_PUBLICATION, p.SCORE_VOTE FROM POST p WHERE p.ID_AUTEUR = :id_utilisateur");
        $statement->bindValue('id_utilisateur', strip_tags($id_utilisateur), PDO::PARAM_STR);

        try {
            $statement->execute();
        } catch (\PDOException $e) {
            return null;
        }

        return $statement;
    }

}
