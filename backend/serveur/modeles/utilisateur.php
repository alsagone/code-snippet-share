<?php
//id_utilisateur, nom_utilisateur, date_inscription, mot_de_passe, adresse_mail, photo
class Utilisateur
{

    private $connexion = null;
    public $id_utilisateur;
    public $nom_utilisateur;
    public $mot_de_passe;
    public $adresse_mail;
    public $photo;

    public function __construct($db)
    {
        $this->connexion = $db;
    }

    public function ajout_utilisateur()
    {
        $statement = $this->connexion->prepare("INSERT INTO UTILISATEUR VALUES (UUID(), :nom_utilisateur, :mot_de_passe, :adresse_mail, CURDATE(), :photo)");
        $statement->bindValue('nom_utilisateur', strip_tags($this->nom_utilisateur), PDO::PARAM_STR);
        $statement->bindValue('mot_de_passe', strip_tags($this->mot_de_passe), PDO::PARAM_STR);
        $statement->bindValue('adresse_mail', strip_tags($this->adresse_mail), PDO::PARAM_STR);
        $statement->bindValue('photo', strip_tags($this->photo), PDO::PARAM_STR);

        try {
            $statement->execute();
        } catch (\PDOException $e) {
            return -1;
        }

        return 0;
    }

    public function modifier_utilisateur()
    {
        $statement = $this->connexion->prepare("UPDATE UTILISATEUR u SET u.NOM_UTILISATEUR = :nom_utilisateur, u.MOT_DE_PASSE = :mot_de_passe , u.ADRESSE_MAIL = :adresse_mail, u.PHOTO = :photo WHERE u.ID_UTILISATEUR = :id_utilisateur");
        $statement->bindValue('id_utilisateur', htmlspecialchars(strip_tags($this->id_utilisateur)), PDO::PARAM_STR);
        $statement->bindValue('nom_utilisateur', htmlspecialchars(strip_tags($this->nom_utilisateur)), PDO::PARAM_STR);
        $statement->bindValue('mot_de_passe', htmlspecialchars(strip_tags($this->mot_de_passe)), PDO::PARAM_STR);
        $statement->bindValue('adresse_mail', htmlspecialchars(strip_tags($this->adresse_mail)), PDO::PARAM_STR);
        $statement->bindValue('photo', htmlspecialchars(strip_tags($this->photo)), PDO::PARAM_STR);

        try {
            $statement->execute();
        } catch (\PDOException $e) {
            return -1;
        }

        return 0;
    }

    public function get_id_utilisateur()
    {
        $statement = $this->connexion->prepare("SELECT u.ID_UTILISATEUR FROM UTILISATEUR u WHERE u.NOM_UTILISATEUR = :nom_utilisateur");
        $nom_utilisateur = "";

        $statement->bindValue('nom_utilisateur', htmlspecialchars(strip_tags($this->nom_utilisateur)), PDO::PARAM_STR);

        try {
            $statement->execute();
            $nom_utilisateur = $statement->fetchColumn();
        } catch (\PDOException $e) {
            return null;
        }

        return $nom_utilisateur;
    }

    public function get_nom_utilisateur($id_utilisateur)
    {
        $statement = $this->connexion->prepare("SELECT u.NOM_UTILISATEUR FROM UTILISATEUR u WHERE u.ID_UTILISATEUR = :id_utilisateur");
        $nom_utilisateur = "";

        $statement->bindValue('id_utilisateur', htmlspecialchars(strip_tags($id_utilisateur)), PDO::PARAM_STR);

        try {
            $statement->execute();
            $nom_utilisateur = $statement->fetchColumn();
        } catch (\PDOException $e) {
            return -1;
        }

        return $nom_utilisateur;
    }

    public function get_hashed_password()
    {
        $statement = $this->connexion->prepare("SELECT u.MOT_DE_PASSE FROM UTILISATEUR u WHERE u.NOM_UTILISATEUR = :nom_utilisateur");

        $statement->bindValue('nom_utilisateur', htmlspecialchars(strip_tags($this->nom_utilisateur)), PDO::PARAM_STR);

        try {
            $statement->execute();
            $hash = $statement->fetchColumn();
        } catch (\PDOException $e) {
            return null;
        }

        return $hash;
    }

    public function get_details()
    {
        $statement = $this->connexion->prepare("SELECT u.ID_UTILISATEUR, u.ADRESSE_MAIL, u.DATE_INSCRIPTION, u.PHOTO FROM UTILISATEUR u WHERE u.NOM_UTILISATEUR = :nom_utilisateur");
        $statement->bindValue('nom_utilisateur', strip_tags($this->nom_utilisateur), PDO::PARAM_STR);

        try {
            $statement->execute();
        } catch (\PDOException $e) {
            return null;
        }

        return $statement;
    }

    public function nom_utilisateur_existe()
    {

        $statement = $this->connexion->prepare("SELECT EXISTS(SELECT 1 FROM UTILISATEUR u WHERE u.NOM_UTILISATEUR = :nom_utilisateur)");
        $statement->bindValue('nom_utilisateur', htmlspecialchars(strip_tags($this->nom_utilisateur)), PDO::PARAM_STR);

        try {
            $statement->execute();
            $count = $statement->fetchColumn();
        } catch (\PDOException $e) {
            return true;
        }

        return $count == 1;
    }

    public function connexion_utilisateur()
    {
        $mdp_clair = htmlspecialchars(strip_tags($this->mot_de_passe));
        $hash = $this->get_hashed_password();
        return password_verify($mdp_clair, $hash);
    }

    public function editer_utilisateur()
    {
        $statement = $this->connexion->prepare("UPDATE UTILISATEUR u SET u.PHOTO = IFNULL(:photo, u.PHOTO) WHERE u.ID_UTILISATEUR = :id_utilisateur");
        $statement->bindValue('photo', $this->photo, PDO::PARAM_STR);
        $statement->bindValue('id_utilisateur', $this->id_utilisateur, PDO::PARAM_STR);

        try {
            $statement->execute();
        } catch (\PDOException $e) {
            echo $e;
            return -1;
        }

        return 0;
    }
}
