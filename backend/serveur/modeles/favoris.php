<?php
class Favoris
{
    private $connexion = null;
    public $id_post;
    public $id_utilisateur;

    public function __construct($db)
    {
        $this->connexion = $db;
    }

    public function ajouter_favori()
    {
        $statement = $this->connexion->prepare("INSERT INTO FAVORIS VALUES (:id_utilisateur, :id_post)");
        $statement->bindValue('id_utilisateur', htmlspecialchars(strip_tags($this->id_utilisateur)), PDO::PARAM_STR);
        $statement->bindValue('id_post', htmlspecialchars(strip_tags($this->id_post)), PDO::PARAM_STR);

        try {
            $statement->execute();
        } catch (\PDOException $e) {
            return -1;
        }

        return 0;
    }

    public function supprimer_favori()
    {
        $statement = $this->connexion->prepare("DELETE FROM FAVORIS f WHERE f.ID_UTILISATEUR = :id_utilisateur AND f.ID_POST = :id_post");
        $statement->bindValue('id_utilisateur', htmlspecialchars(strip_tags($this->id_utilisateur)), PDO::PARAM_STR);
        $statement->bindValue('id_post', htmlspecialchars(strip_tags($this->id_post)), PDO::PARAM_STR);

        try {
            $statement->execute();
        } catch (\PDOException $e) {
            return -1;
        }

        return 0;
    }

    public function get_nb_favoris_post()
    {
        $statement = $this->connexion->prepare("SELECT COUNT(*) FROM FAVORIS f WHERE f.ID_POST = :id_post");
        $count = 0;

        $statement->bindValue('id_post', htmlspecialchars(strip_tags($this->id_post)), PDO::PARAM_STR);

        try {
            $statement->execute();
            $count = $statement->fetchColumn();
        } catch (\PDOException $e) {
            return -1;
        }

        return $count;
    }

    public function get_favoris_utilisateur()
    {
        $statement = $this->connexion->prepare("SELECT * FROM FAVORIS f WHERE f.ID_UTILISATEUR = :id_utilisateur");
        $statement->bindValue('id_utilisateur', htmlspecialchars(strip_tags($this->id_utilisateur)), PDO::PARAM_STR);

        try {
            $statement->execute();
        } catch (\PDOException $e) {
            return null;
        }

        return $statement;
    }
}

