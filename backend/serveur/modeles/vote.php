<?php
class Vote
{
    private $connexion = null;
    public $id_post;
    public $id_utilisateur;

    public function __construct($db)
    {
        $this->connexion = $db;
    }

    public function voter($valeur)
    {
        if ($valeur !== 1 && $valeur !== -1) {
            return -1;
        }

        $statement = $this->connexion->prepare("INSERT INTO VOTE VALUES (:id_utilisateur, :id_post, :valeur)");
        $statement->bindValue('id_utilisateur', htmlspecialchars(strip_tags($this->id_utilisateur)), PDO::PARAM_STR);
        $statement->bindValue('id_post', htmlspecialchars(strip_tags($this->id_post)), PDO::PARAM_STR);
        $statement->bindValue('valeur', $valeur, PDO::PARAM_INT);

        try {
            $statement->execute();
        } catch (\PDOException $e) {
            return -1;
        }

        return 0;
    }


    public function upvote()
    {
        return $this->voter(1);
    }

    public function downvote()
    {
        return $this->voter(-1);
    }

    public function supprimer_vote()
    {
        $statement = $this->connexion->prepare("DELETE FROM VOTE v WHERE v.ID_UTILISATEUR = :id_utilisateur AND v.ID_POST = :id_post LIMIT 1");
        $statement->bindValue('id_utilisateur', htmlspecialchars(strip_tags($this->id_utilisateur)), PDO::PARAM_STR);
        $statement->bindValue('id_post', htmlspecialchars(strip_tags($this->id_post)), PDO::PARAM_STR);

        try {
            $statement->execute();
        } catch (\PDOException $e) {
            return -1;
        }

        return 0;
    }

    public function get_nb_upvotes()
    {
        $statement = $this->connexion->prepare("SELECT COUNT(*) FROM VOTE v WHERE v.ID_POST = :id_post AND v.VALEUR = 1");
        $count = 0;

        $statement->bindValue('id_post', htmlspecialchars(strip_tags($this->id_post)), PDO::PARAM_STR);

        try {
            $statement->execute();
            $count = $statement->fetchColumn();
        } catch (\PDOException $e) {
            return -1;
        }

        return $count;
    }

    public function get_nb_downvotes()
    {
        $statement = $this->connexion->prepare("SELECT COUNT(*) FROM VOTE v WHERE v.ID_POST = :id_post AND v.VALEUR = -1");
        $count = 0;

        $statement->bindValue('id_post', htmlspecialchars(strip_tags($this->id_post)), PDO::PARAM_STR);

        try {
            $statement->execute();
            $count = $statement->fetchColumn();
        } catch (\PDOException $e) {
            return -1;
        }

        return $count;
    }
}
