import random
from random import randrange
from datetime import timedelta, datetime


def get_file_path(filename: str):
    return f"/var/lib/mysql-files/{filename}"


dict_extensions = {
    ".c": "C",
    ".css": "CSS",
    ".html": "HTML",
    ".java": "Java",
    ".js": "JavaScript",
    ".php": "PHP",
    ".py": "Python",
    ".rs": "Rust",
    ".sql": "MySQL",
    ".ts": "TypeScript"
}

liste_fichiers = [
    "c.c",
    "css.css",
    "html.html",
    "java.java",
    "js.js",
    "php.php",
    "python.py",
    "rust.rs",
    "sql.sql",
    "typescript.ts"
]

users = ["alsagone", "PoisoonB",
         "RejectedCryptid", "cletus0297", "DeviantNewt"]

user_ids = {
    "alsagone": '252a436d-abf4-11ee-a869-142d27d3279f',
    "PoisoonB": '252dad10-abf4-11ee-a869-142d27d3279f',
    "RejectedCryptid": '252f2697-abf4-11ee-a869-142d27d3279f',
    "cletus0297": '25303676-abf4-11ee-a869-142d27d3279f',
    "DeviantNewt": '25318949-abf4-11ee-a869-142d27d3279f'
}

date_debut = datetime.strptime('12/1/2023 1:00 AM', '%m/%d/%Y %I:%M %p')
date_fin = datetime.strptime('1/5/2024 11:59 PM', '%m/%d/%Y %I:%M %p')


def get_langage(filename: str):
    global dict_extensions
    extension = filename[filename.index('.'):]
    return dict_extensions[extension]


def get_file_contents(filename: str):
    with open(filename, "r") as f:
        content = f.read()

    return content


def random_date(start, end):
    """
    This function will return a random datetime between two datetime 
    """
    delta = end - start
    int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
    random_second = randrange(int_delta)
    return start + timedelta(seconds=random_second)


def post_insert(titre: str, filename: str, id_auteur: str) -> str:
    file_path = get_file_path(filename)
    langage = get_langage(filename)
    d = random_date(date_debut, date_fin)
    return f"(UUID(), '{titre}', LOAD_FILE('{file_path}'), '{langage}', '{id_auteur}', '{d}', 0)"


def creer_instructions(nb_posts: int):
    instruction = "INSERT INTO POST VALUES\n"

    for i in range(1, nb_posts + 1):
        titre = f"Post numero {i}"
        username = random.choice(users)
        id_auteur = user_ids[username]
        fichier = random.choice(liste_fichiers)

        separator = ",\n" if i < nb_posts else ";"
        instruction += post_insert(titre, fichier, id_auteur) + separator

    with open("posts.sql", "w") as f:
        f.write(instruction)

    return


if __name__ == "__main__":
    creer_instructions(50)
