// Append content to a file 

Jaxenter out = null;
try {
	out = new Jaxenter (new FileWriter(”filename”, true));
	out.write(”aString”);
} catch (IOException e) {
	// error processing code
} finally {
	if (out != null) {
		out.close();
	}
}
