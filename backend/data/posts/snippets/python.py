def encode_char(char: str, rot_value: int = 13) -> str:
    if not (char.isalpha()):
        return char

    n = ord('a') if char.islower() else ord('A')
    new_ascii_code = (ord(char) - n + rot_value) % 26 + n
    return chr(new_ascii_code)


def decode_char(char: str, rot_value: int = 13) -> str:
    if not (char.isalpha()):
        return char

    n = ord('a') if char.islower() else ord('A')
    new_ascii_code = (ord(char) - n - rot_value) % 26 + n
    return chr(new_ascii_code)


def rot_n(string: str, n: int = 13) -> str:
    encoded_str = ""

    for char in string:
        encoded_str += encode_char(char, n)

    return encoded_str


def derot_n(string: str, n: int = 13) -> str:
    decoded_str = ""

    for char in string:
        decoded_str += decode_char(char, n)

    return decoded_str


if __name__ == "__main__":
    print(rot_n('Test') == 'Grfg')
    print(derot_n('Grfg') == 'Test')
