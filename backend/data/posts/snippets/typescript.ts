const wrapNumber = (n: number, a: number, b: number) => {
  let min: number, max: number;
  if (a < b) {
    min = a;
    max = b;
  } else {
    min = b;
    max = a;
  }

  return ((n - min) % (max - min + 1)) + min;
};
