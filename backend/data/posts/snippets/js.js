// Difference between two arrays

const difference = (a, b) => {
  const setA = new Set(a);
  const setB = new Set(b);

  return [...a.filter((x) => !setB.has(x)), ...b.filter((x) => !setA.has(x))];
};
