liste_users = [
    {
        "username": "alsagone",
        "user_id": 67,
        "hash": '$2a$12$YPFB0PXXeiQmhksyk/R5G.l.pYk6DQbW44QwlppXbEFgxgpNxhAxm'
    },
    {
        "username": "PoisoonB",
        "user_id": 4,
        "hash": '$2a$12$CFQDarB7r26im11L8NTKFuUapX223ARGGl1gErU0U0MCBdMgaPr66'
    },
    {
        "username": "RejectedCryptid",
        "user_id": 69,
        "hash": '$2a$12$serX6er0YJEpTK41F4sJTu4kEQG4Eh/telQFSQI3jr4nRRuY2j0Ey'
    },
    {
        "username": "cletus0297",
        "user_id": 297,
        "hash": '$2a$12$v7MjykhjxFhX1aQRs2ITEerZcrqRR4F4nZIvbUqq22e97TFcRm0F.'
    },
    {
        "username": "DeviantNewt",
        "user_id": 87,
        "hash": '$2a$12$jXdAw4xGGGHa5H0DTb1g6OADkfBAvp/lVLL08/6bbQonjh6HPyC8C'
    }
]


def get_profile_picture(username: str):
    return f'/var/lib/mysql-files/{username}.jpg'


def insertion_user(user) -> str:
    user_id = user["user_id"]
    username = user["username"]
    pwd = user["hash"]

    email = f'{username}@protonmail.com'
    photo = get_profile_picture(username)
    return f"(UUID(), '{username}', '{pwd}', '{email}', CURRENT_DATE - INTERVAL FLOOR(RAND() * 12) DAY, LOAD_FILE('{photo}'))"


def creer_instructions():
    instruction = "INSERT INTO UTILISATEUR VALUES\n"

    for (index, user) in enumerate(liste_users):
        separator = ",\n" if index < len(liste_users) - 1 else ";"
        instruction += insertion_user(user) + separator

    with open("users.sql", "w") as f:
        f.write(instruction)
    return


if __name__ == "__main__":
    creer_instructions()
