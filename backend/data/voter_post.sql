CREATE DEFINER=`root`@`localhost` PROCEDURE `CODE_SNIPPET`.`VOTER_POST`(IN id_post CHAR(36), IN id_utilisateur CHAR(36), IN valeur_vote INT)

BEGIN
	DECLARE valeur_vote_existant INT DEFAULT 0;

	IF (valeur_vote != 1 AND valeur_vote != -1) THEN 
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Valeur de vote incorrecte';
	END IF;

	SELECT COALESCE(v.VALEUR_VOTE, 0) INTO @valeur_vote_existant FROM VOTE v WHERE v.ID_POST = id_post AND v.ID_UTILISATEUR = id_utilisateur;

	-- Si valeur_vote_existant != 0 alors l'utilisateur a déjà voté donc on revert le vote
	IF (@valeur_vote_existant != 0) THEN 
		START TRANSACTION;
			UPDATE VOTE v SET v.VALEUR_VOTE = valeur_vote WHERE v.ID_POST = id_post AND v.ID_UTILISATEUR = id_utilisateur;
			UPDATE POST p SET p.SCORE_VOTE = p.SCORE_VOTE - @valeur_vote_existant WHERE p.ID_POST = id_post;
		COMMIT;
	END IF;


	INSERT INTO VOTE VALUES (id_utilisateur, id_post, valeur_vote);

	START TRANSACTION;
	UPDATE POST p SET p.SCORE_VOTE  = p.SCORE_VOTE  + valeur_vote WHERE p.ID_POST = id_post;
	COMMIT;
	
END