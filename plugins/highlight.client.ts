import "highlight.js/styles/monokai.min.css";
import hljs from "highlight.js/lib/core";
import javascript from "highlight.js/lib/languages/javascript";
import hljsVuePlugin from "@highlightjs/vue-plugin";
import { install as VueMonacoEditorPlugin } from "@guolao/vue-monaco-editor";

export default defineNuxtPlugin((nuxtApp) => {
  hljs.registerLanguage("javascript", javascript);
  nuxtApp.vueApp.use(hljsVuePlugin);
  nuxtApp.vueApp.use(VueMonacoEditorPlugin, {
    paths: {
      // The recommended CDN config
      vs: "https://cdn.jsdelivr.net/npm/monaco-editor@0.43.0/min/vs",
    },
  });
});
